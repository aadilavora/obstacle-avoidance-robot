Following project was meant to be optimize path planning of robots in material handling environment.

Objective of this project was to improve efficiency of robots moving materials by reducing the traffic and keeping their speed constant while avoiding the collision at all costs. 

Alogorithm was developed for the same purpose.
As a first step we perfromed a MATLAB simulation with wide range of inputs and ensured validity of the algorithm. Matlab code is attached as "obstacle avoidance_simulation.m"

The next step was develop a physical model inorder to test. We were only able to test the static obstacles due to time constraints. We used a wheeled robot with DC motors and used arduino to control it. "obstacle_avoidance_arduino_code.ino" is the attached arduino code .

Broad level overview of the project is explained in the attached powerpoint presentation "Simulation and Modeling of Wheeled Mobile Robot.pptx"

