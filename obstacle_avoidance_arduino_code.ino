

int ENA=5;//connected to Arduino's port 5(output pwm)
int IN1=2;//connected to Arduino's port 2
int IN2=3;//connected to Arduino's port 3

int ENB=6;//connected to Arduino's port 6(output pwm)
int IN3=4;//connected to Arduino's port 4
int IN4=7;//connected to Arduino's port 7

int sensorPin=12;//connected to o/p of sensor
int led=13;
int buzzer=8;//Connect the buzzer positive Pin to Digital Pin 8 and negative pin to GND 

void setup()

{
 pinMode(ENA,OUTPUT);//output
 pinMode(ENB,OUTPUT);
 pinMode(IN1,OUTPUT);
 pinMode(IN2,OUTPUT);
 pinMode(IN3,OUTPUT); 
 pinMode(IN4,OUTPUT);
 pinMode(buzzer,OUTPUT);//Set Pin Mode as output
 digitalWrite(ENA,LOW);
 digitalWrite(ENB,LOW);//stop driving
 digitalWrite(IN1,LOW); 
 digitalWrite(IN2,HIGH);//setting motorA's directon  
 digitalWrite(IN3,HIGH);                            
 digitalWrite(IN4,LOW);//setting motorB's directon
 Serial.begin(9600);
}

void loop()

{
  
  int d=250;
  int sensorValue=digitalRead(sensorPin);
  analogRead(0);
  digitalWrite(led,sensorValue);
  delay(d);
  Serial.println(sensorValue);

if(sensorValue==1)
  {
    //for right turn
  digitalWrite(buzzer,HIGH);
  digitalWrite(IN1,HIGH); 
 digitalWrite(IN2,LOW);//setting motorA's directon  
 digitalWrite(IN3,HIGH);                            
 digitalWrite(IN4,LOW);//setting motorB's directon
  analogWrite(ENA,255);   //slip steering
  analogWrite(ENB,255);
  delay(2000); //5000
  digitalWrite(buzzer,LOW);
  //for continuing motion
digitalWrite(IN1,HIGH); 
 digitalWrite(IN2,LOW);//setting motorA's directon  
 digitalWrite(IN3,LOW);                            
 digitalWrite(IN4,HIGH);//setting motorB's directon
  analogWrite(ENA,255);//start driving motorA
  analogWrite(ENB,255);//start driving motorB
  delay(5000); //7000
//for turning left
  digitalWrite(buzzer,HIGH);
  digitalWrite(IN1,LOW); 
 digitalWrite(IN2,HIGH);//setting motorA's directon  
 digitalWrite(IN3,LOW);                            
 digitalWrite(IN4,HIGH);//setting motorB's directon
  analogWrite(ENA,255);   //slip steering
  analogWrite(ENB,255);
  delay(2000); //5000
  digitalWrite(buzzer,LOW);
  //for continuing motion
digitalWrite(IN1,HIGH); 
 digitalWrite(IN2,LOW);//setting motorA's directon  
 digitalWrite(IN3,LOW);                            
 digitalWrite(IN4,HIGH);//setting motorB's directon
  analogWrite(ENA,255);//start driving motorA
  analogWrite(ENB,255);//start driving motorB
delay(8000); //10000
//for turning left
  digitalWrite(buzzer,HIGH);
  digitalWrite(IN1,LOW); 
 digitalWrite(IN2,HIGH);//setting motorA's directon  
 digitalWrite(IN3,LOW);                            
 digitalWrite(IN4,HIGH);//setting motorB's directon
  analogWrite(ENA,255);   //slip steering
  analogWrite(ENB,255);
delay(2000); //5000
  digitalWrite(buzzer,LOW);
  //for continuing motion
digitalWrite(IN1,HIGH); 
 digitalWrite(IN2,LOW);//setting motorA's directon  
 digitalWrite(IN3,LOW);                            
 digitalWrite(IN4,HIGH);//setting motorB's directon
  analogWrite(ENA,255);//start driving motorA
  analogWrite(ENB,255);//start driving motorB
  delay(5000); //7000
    //for right turn
   digitalWrite(buzzer,HIGH);
   digitalWrite(IN1,HIGH); 
 digitalWrite(IN2,LOW);//setting motorA's directon  
 digitalWrite(IN3,HIGH);                            
 digitalWrite(IN4,LOW);//setting motorB's directon
  analogWrite(ENA,255);   //slip steering
  analogWrite(ENB,255);
  delay(2000); //4000
  digitalWrite(buzzer,LOW);
}
  else
  {
 digitalWrite(IN1,HIGH); 
 digitalWrite(IN2,LOW);//setting motorA's directon  
 digitalWrite(IN3,LOW);                            
 digitalWrite(IN4,HIGH);//setting motorB's directon
  analogWrite(ENA,255);//start driving motorA
  analogWrite(ENB,255);//start driving motorB
  }
}
